# CREATING VPC #
resource "aws_vpc" "Project_2_vpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "Project_2_vpc"
  }
}

# PUBLIC SUBNETS #
# Public subnet 1
resource "aws_subnet" "Public_subnet1" {
  vpc_id     = aws_vpc.Project_2_vpc.id
  cidr_block = var.Public_subnet1_cidr
  availability_zone = var.AZ_2a

  tags = {
    Name = "Public_subnet1"
  }
}


# Public subnet 2
resource "aws_subnet" "Public_subnet2" {
  vpc_id     = aws_vpc.Project_2_vpc.id
  cidr_block = var.Public_subnet2_cidr
  availability_zone = var.AZ_2b

  tags = {
    Name = "Public_subnet2"
  }
}

# Public subnet 3
resource "aws_subnet" "Public_subnet3" {
  vpc_id     = aws_vpc.Project_2_vpc.id
  cidr_block = var.Public_subnet3_cidr
  availability_zone = var.AZ_2c

  tags = {
    Name = "Public_subnet3"
  }
}

# PRIVATE SUBNETS

# Private subnet 1
resource "aws_subnet" "Private_subnet1" {
  vpc_id     = aws_vpc.Project_2_vpc.id
  cidr_block = var.Private_subnet1_cidr
  availability_zone = var.AZ_2a

  tags = {
    Name = "Private_subnet1"
  }
}

# Private subnet 2
resource "aws_subnet" "Private_subnet2" {
  vpc_id     = aws_vpc.Project_2_vpc.id
  cidr_block = var.Private_subnet2_cidr
  availability_zone = var.AZ_2b

  tags = {
    Name = "Private_subnet2"
  }
}

# Private subnet 3
resource "aws_subnet" "Private_subnet3" {
  vpc_id     = aws_vpc.Project_2_vpc.id
  cidr_block = var.Private_subnet3_cidr
  availability_zone = var.AZ_2c

  tags = {
    Name = "Private_subnet3"
  }
}

# ROUTE TABLES

# Public route table
resource "aws_route_table" "Public_route_table" {
  vpc_id = aws_vpc.Project_2_vpc.id

  tags = {
    Name = "Public_route_table"
  }
}


# Private route table
resource "aws_route_table" "Private_route_table" {
  vpc_id = aws_vpc.Project_2_vpc.id

  tags = {
    Name = "Private_route_table"
  }
}

# PUBLIC ROUTE TABLE ASSOCIATIONS #
# Associating Public subnet 1 to public route table
resource "aws_route_table_association" "Public_subnet1_assoc" {
  subnet_id      = aws_subnet.Public_subnet1.id
  route_table_id = aws_route_table.Public_route_table.id
}

# Associating Public subnet 2 to public route table
resource "aws_route_table_association" "Public_subnet2_assoc" {
  subnet_id      = aws_subnet.Public_subnet2.id
  route_table_id = aws_route_table.Public_route_table.id
}

# Associating Public subnet 3 to public route table
resource "aws_route_table_association" "Public_subnet3_assoc" {
  subnet_id      = aws_subnet.Public_subnet3.id
  route_table_id = aws_route_table.Public_route_table.id
}

# PRIVATE ROUTE TABLE ASSOCIATIONS #
# Associating Private subnet 1 to private route table
resource "aws_route_table_association" "Private_subnet1_assoc" {
  subnet_id      = aws_subnet.Private_subnet1.id
  route_table_id = aws_route_table.Private_route_table.id
}

# Associating Private subnet 2 to private route table
resource "aws_route_table_association" "Private_subnet2_assoc" {
  subnet_id      = aws_subnet.Private_subnet2.id
  route_table_id = aws_route_table.Private_route_table.id
}

# Associating Private subnet 3 to private route table
resource "aws_route_table_association" "Private_subnet3_assoc" {
  subnet_id      = aws_subnet.Private_subnet3.id
  route_table_id = aws_route_table.Private_route_table.id
}


# INTERNET GATEWAY #
resource "aws_internet_gateway" "Project_2_igw" {
  vpc_id = aws_vpc.Project_2_vpc.id

  tags = {
    Name = "Project_2_igw"
  }
}

# Associating igw to the public route table (** Using aws Terraform route resource)
resource "aws_route" "public_route_table_assoc" {
  route_table_id         = aws_route_table.Public_route_table.id
  gateway_id             = aws_internet_gateway.Project_2_igw.id
  destination_cidr_block = var.public_route_table_assoc_cidr
}