resource "aws_db_instance" "MySQL" {
  allocated_storage    = 20
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mysql_rds"
  username             = "Project2"
  password             = "Project2"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
}

# RDS Subnet Group

resource "aws_db_subnet_group" "MySQL_Group" {
  name       = "mysql_rds"
  subnet_ids = [aws_subnet.Private_subnet1.id, aws_subnet.Private_subnet2.id, aws_subnet.Private_subnet3.id]

  tags = {
    Name = "MySQL subnet group"
  }
}

# MySQL Security name
resource "aws_security_group" "MySQL_Secgrp" {
  name        = "mysql_secgrp"
  description = "Allow MySQL inbound traffic"
  vpc_id      = aws_vpc.Project_2_vpc.id

  ingress {
    description      = "TLS from MySQL"
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

