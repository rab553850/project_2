# Creating Application Load Balancer
resource "aws_lb" "P2_ALB" {
  name               = "Application-LB"
  internal           = true
  load_balancer_type = "application"
  security_groups    = [aws_security_group.Secgrp.id]
  subnets            = [aws_subnet.Public_subnet1.id, aws_subnet.Public_subnet2.id, aws_subnet.Public_subnet3.id]
}

# Target group
resource "aws_lb_target_group" "P2_ALB_TG" {
  name        = "P2-ALB-TG"
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.Project_2_vpc.id

  health_check {
    interval = 30
    path  = "/"
    protocol = "HTTP"
    timeout = 5
    healthy_threshold = 5
    unhealthy_threshold = 2
  }

}

# Listeners
resource "aws_lb_listener" "ALB_listener" {
  load_balancer_arn = aws_lb.P2_ALB.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.P2_ALB_TG.arn
  }
}
